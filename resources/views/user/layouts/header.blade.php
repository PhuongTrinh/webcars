<div class="" style="background:#D3D3D3">
      <div class="container-fluid">
      <div class="onHeader ">
          <div class="title">
              <h2>Wellcome to HONDA Cars</h2>
          </div>
          <ul>
              <li>
                  <a href="#">
                      <i class="fa fa-facebook"></i>
                  </a>
              </li>
              <li>
                  <a href="#">
                      <i class="fa fa-github"></i>
                  </a>
              </li>
              <li>
                  <a href="#">
                      <i class="fa fa-twitter"></i>
                  </a>
              </li>
              <li>
                  <a href="#">
                      <i class="fa fa-google"></i>
                  </a>
              </li>
          </ul>
      </div>
      </div>
 </div>
<header class="header">
    
  <div class="container-fluid">
    <nav class="navbar navbar-expand-lg">
      <a class="navbar-brand" href="#">
        <img src="image/car/logo.png">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02"
       aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse " id="navbarTogglerDemo02" style="height:100%">
        <ul class="navbar-nav ml-auto ">
          <li class="nav-item active">
            <a class="nav-link" href="#home">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#new">News & Event</a>
          </li>
          <li class="nav-item dropdown show">
                          <a class=" nav-link  dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Product
                          </a>
                         <div class="dropdown-menu mydropdown animated fadeInUp" aria-labelledby="dropdownMenuLink">
                              <a class="dropdown-item" href="product">All Cars</a>
                              <a class="dropdown-item" href="#">Top Cars</a>
                              <a class="dropdown-item" href="#">New Cars</a>
                          </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#contactUs">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#">Login/Logout</a>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</header>
    