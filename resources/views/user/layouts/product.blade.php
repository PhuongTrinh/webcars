@extends('user.user')
@section('content')
	<!-- all product -->
    <div class="page">
        <div class="container">
            <p>Home/product</p>
        </div>
    </div>

    <section class="mt-5">
        <div class="Title mb-3">
            <h1>All Product</h1>
        </div>
        <div class="container">
            <div class="row" id="allCar">
                <div class="col-3">
                    <div class="card cardContent">
                        <img src="image/car/xe-1.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-outline-primary">View more</button>
                                <button class="btn btn-outline-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent">
                        <img src="image/car/xe-1.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-outline-primary">View more</button>
                                <button class="btn btn-outline-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent">
                        <img src="image/car/xe-1.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-outline-primary">View more</button>
                                <button class="btn btn-outline-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent">
                        <img src="image/car/xe-1.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-outline-primary">View more</button>
                                <button class="btn btn-outline-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card cardContent mt-3">
                        <img src="image/car/xe-3.png" alt="">
                        <div class="card-body cardBody">
                            <!-- name car -->
                            <h3 class=""><a href="#">black cars</a></h3>
                            <p>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </p>
                            <!-- code and price -->
                            <div class="code_price">
                                <p>code: 111</p>
                                <p>price: $190</p>
                            </div>
                            <div class=" button">
                                <button class="btn btn-outline-primary">View more</button>
                                <button class="btn btn-outline-dark"><i class="fa fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection