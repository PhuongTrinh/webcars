@extends('user.user')
@section('content')


  <section >
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active img">
          <img class="d-block w-100" src="image/car/banner-3.jpg" alt="First slide">
        </div>
        <div class="carousel-item img">
          <img class="d-block w-100" src="http://www.st8.vn/data/news/1185/2.jpg" alt="Second slide">
        </div>
        <div class="carousel-item img">
          <img class="d-block w-100" src="image/car/banner-4.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </section>
  
  <!-- introduce -->
  
  <section class="intro" id="about">
    <div class="container">
      <div class="mt-5 Title">
        <h1>Introduce</h1>
      </div>
      <div class="row">
        <div class="col-lg-6 introImg mt-3">
          <div class="img">
            <img src="image/car/intro-3.jpg" alt="">
            <div class="button">
              <button class=" btn-primary">View More</button>
            </div>
            <div class="unknow">
              <i class="fa fa-arrows p-2"></i>
            </div>
          </div>
          <div class="introContent">
            <h3>Car buying and selling service </h3>
            <p class="my-3">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis doloribus animi, laudantium 
              odit minima recusandae at quo facilis. Consequatur, saepe.</p>
              
          </div>
        </div>
        <div class="col-lg-6 introImg mt-3">
          <div class="img">
            <img src="image/car/intro-4.jpg" alt="">
            <div class="button">
              <button class=" btn-primary">View More</button>
            </div>
            <div class="unknow">
              <i class="fa fa-arrows p-2"></i>
            </div>
          </div>
          <div class="introContent">
            <h3>Modern equipments </h3>
            <p class="my-3">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis doloribus animi, laudantium 
              odit minima recusandae at quo facilis. Consequatur, saepe.</p>  
          </div>
        </div>
        <div class="col-lg-6 introImg mt-3">
          <div class="img">
            <img src="https://s3.amazonaws.com/atlautomotivewebsite/wp-content/uploads/2018/03/04173559/honda-civic-banner-7.jpg" alt="">
            <div class="button">
              <button class=" btn-primary">View More</button>
            </div>
            <div class="unknow">
              <i class="fa fa-arrows p-2"></i>
            </div>
          </div>
          <div class="introContent">
            <h3>Super level </h3>
            <p class="my-3">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis doloribus animi, laudantium 
              odit minima recusandae at quo facilis. Consequatur, saepe.</p>
          </div>
        </div>
        <div class="col-lg-6 introImg mt-3">
          <div class="img">
            <img src="https://di-uploads-pod5.dealerinspire.com/newenglandhonda/uploads/2017/12/2018-Honda-Civic-Sedan-Daytime-Running-Lamps-.jpg" alt="">
            <div class="button">
              <button class=" btn-primary">View More</button>
            </div>
            <div class="unknow">
              <i class="fa fa-arrows p-2"></i>
            </div>
          </div>
          <div class="introContent">
            <h3>Delicate to every detail </h3>
            <p class="my-3">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis doloribus animi, laudantium 
              odit minima recusandae at quo facilis. Consequatur, saepe.</p>
          </div>
        </div>
        
      </div>
    </div>
  </section>

  <!-- product -->
  <section class="product">
    <div class="my-5 Title">
      <h1> New Products</h1>
    </div>
    <div class="proContent">
      <div class="container">
        <div class="owl-carousel owl-theme myOwl1">
          <div class="item owlItem" style="border:1px solid green">
            <div class="card cardImg">
              <img src="image/car/xe-3-hover.png">
              <div class="card-body  text-center">
                <h2 class="card-title text-success">
                  Cars Sedans
                  <span class="badge badge-success badge-pill">12</span>
                </h2>
                <div class="d-flex justify-content-around">
                  <div class="product-left">
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>
                  <div>
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>  
                </div>
                <button class="btn btn-success">View more</button>
                <button class="btn btn-outline-success">Buy</button>
              </div>
            </div>
          </div>
          <div class="item owlItem" style="border:1px solid blue">
            <div class="card cardImg">
              <img src="image/car/xe-2-hover.png">
              <div class="card-body  text-center">
              <h2 class="card-title text-primary">
                  Cars Sedans
                  <span class="badge badge-primary badge-pill">12</span>
                </h2>
                <div class="d-flex justify-content-around">
                  <div class="product-left">
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>
                  <div>
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>  
                </div>
                <button class="btn btn-success">View more</button>
                <button class="btn btn-outline-success">Buy</button>
              </div>
            </div>
          </div>
          <div class="item owlItem" style="border:1px solid black">
            <div class="card cardImg">
              <img src="image/car/xe-1-hover.png">
              <div class="card-body  text-center">
              <h2 class="card-title text-dark ">
                  Cars Sedans
                  <span class="badge badge-dark badge-pill">12</span>
                </h2>
                <div class="d-flex justify-content-around">
                  <div class="product-left">
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>
                  <div>
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>  
                </div>
                <button class="btn btn-success">View more</button>
                <button class="btn btn-outline-success">Buy</button>
              </div>
            </div>
          </div>
          <div class="item owlItem" style="border:1px solid red">
            <div class="card cardImg">
              <img src="image/car/xe-2.png">
              <div class="card-body  text-center">
              <h2 class="card-title text-danger">
                  Cars Sedans
                  <span class="badge badge-danger badge-pill">12</span>
                </h2>
                <div class="d-flex justify-content-around">
                  <div class="product-left">
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>
                  <div>
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>  
                </div>
                <button class="btn btn-success">View more</button>
                <button class="btn btn-outline-success">Buy</button>
              </div>
            </div>
          </div>
          <div class="item owlItem" style="border:1px solid blueviolet">
            <div class="card cardImg">
              <img src="image/car/xe-3.png">
              <div class="card-body  text-center">
              <h2 class="card-title " style="color:blueviolet">
                  Cars Sedans
                  <span class="badge badge-primary badge-pill">12</span>
                </h2>
                <div class="d-flex justify-content-around">
                  <div class="product-left">
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>
                  <div>
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>  
                </div>
                <button class="btn btn-success">View more</button>
                <button class="btn btn-outline-success">Buy</button>
              </div>
            </div>
          </div>
          <div class="item owlItem" style="border:1px solid orange">
            <div class="card cardImg">
              <img src="image/car/xe-4.png">
              <div class="card-body text-center">
                <h2 class="card-title text-warning">
                  Cars Sedans
                  <span class="badge badge-warning badge-pill">12</span>
                </h2>
                <div class="d-flex justify-content-around">
                  <div class="product-left">
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>
                  <div>
                    <p>$410,000</p>
                    <p>Starting</p>
                  </div>  
                </div>
                <button class="btn btn-success">View more</button>
                <button class="btn btn-outline-success">Buy</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="view_more">
        <button>
          <a href="product">View all cars</a>
        </button>
      </div>
    </div>
    
  </section>

  <!-- background -->
  <section class="background">
    <div class="bgContent">
      <div class="infoCompany">
      <div class="container ">
        <div class="row ">
          <div class="col-lg-3">
            <h1 class="text-warning">929 </h1>
            <span class=""> 929 City car was bought, accounted for 49% of total sales</span>
          </div>
          <div class="col-lg-3">
            <h1 class="text-warning">341</h1>
            <span class="">341 Jazz car was bought, accounted for 18% of total sales</span>
          </div>
          <div class="col-lg-3">
            <h1 class="text-warning"> 1.914 </h1>
            <span>1.914 Honda car was bought, up 169% over the same period last year</span>
          </div>
          <div class="col-lg-3">
            <h1 class="text-warning">7 </h1>
            <span>7 kind of car popular ,beautiful and luxurious</span>
          </div>
        </div>
      </div>
      </div>
    </div>
  </section>

  <!-- event and news -->
  <section id="new">
    <div class="my-5 Title">
      <h1>News and Event</h1>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-6 mt-4">
          <div class="row EN">
            <div class="col-lg-4 left">
              <img src="https://cdn.partcatalog.com/media/wysiwyg/tesla-model-3/tesla-model-3-banner.jpg" class="img-fluid">
            </div>
            <div class="col-lg-8 right">
              <h3>
                <a href="#">Sedans cars</a>
              </h3>
              <span>Sales 5%</span>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis, harum.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mt-4">
          <div class="row EN">
            <div class="col-lg-4 left">
              <img src="https://cdn.partcatalog.com/media/wysiwyg/tesla-model-3/tesla-model-3-banner.jpg" class="img-fluid">
            </div>
            <div class="col-lg-8 right">
              <h3><a href="#">Sedans cars</a></h3>
              <span>Sales 5%</span>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis, harum.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mt-4">
          <div class="row EN">
            <div class="col-lg-4 left">
              <img src="https://cdn.partcatalog.com/media/wysiwyg/tesla-model-3/tesla-model-3-banner.jpg" class="img-fluid">
            </div>
            <div class="col-lg-8 right">
              <h3><a href="#">Sedans cars</a></h3>
              <span>Sales 5%</span>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis, harum.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mt-4">
          <div class="row EN">
            <div class="col-lg-4 left">
              <img src="https://cdn.partcatalog.com/media/wysiwyg/tesla-model-3/tesla-model-3-banner.jpg" class="img-fluid">
            </div>
            <div class="col-lg-8 right">
              <h3><a href="#">Sedans cars</a></h3>
              <span>Sales 5%</span>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis, harum.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <!-- contact for us -->
  <section class="contact" id="contact">
    <div class="my-5 Title">
      <h1>Contact for us</h1>
    </div>
    <div class="container">
      <div class="FormContact" >
        <div class="left">
          <img src="https://di-uploads-pod4.dealerinspire.com/montanahondadealers/uploads/2017/09/Accordion-Image-2018-Honda-Civic-Hatchback-EX-L.png" class="img-fluid">
        </div>
        <div class=" right" >
        
          <!-- <div class="form">
            <div class="TitleForm">
              <h3>Form contact</h3>
            </div>
            <form>
            
              <div class="form-group">
                <label>Full Name</label>
                <input type="text" placeholder="your name" class="form-control">
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="text" placeholder="your email" class="form-control">
              </div>
              <div class="form-group">
                <label>Phone</label>
                <input type="text" placeholder="your phone numer" class="form-control">
              </div>
              <div class="form-group">
                <label for="Message"></label>
                <textarea name="" id="" cols="30" rows="5" placeholder="your message" class="form-control"></textarea>
              </div>
              <div class="form-group">
                <button class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div> -->

          <form class="FormLogin">
            <div class="" style="display:flex;justify-content:space-between">
              <div class="UserName ">
                <input type="text" name="UserName" class="user general">
                <label for="" id="l1" class="lb">Name</label>
              </div>
              <div class="PhoneNumber ">
                <input type="text" name="PhoneNumber" class="phone general">
                <label for="" id="l4" class="lb">Phone</label>
              </div>
            </div>
            <div class="PassWord">
              <input type="email" name="PassWord" class="email general">
              <label for="" id="l2" class="lb">Email</label>
            </div>
            <div class="message">
              <!-- <input type="password" name="PassWord" class="pass"> -->
              <textarea name="message" id="" cols="30" rows="4" class="message general"></textarea>
              <label for="" id="l3" class="lb">Message</label>
            </div>
            <div class="text-center pt-4">
              <button type="button" class="btn btn-outline-success" onclick="Submit();">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-4 left">
          <div class="img">
            <img src="https://www.westernwashingtonhondadealers.com/images/logo-dealer.png">
          </div>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta nisi, ipsum culpa porro
             distinctio nulla eligendi suscipit amet quo nihil.</p>
          <div class="social">
            <div class="bg-primary">
              <a href="#"><i class="fa fa-facebook"></i></a>
            </div>
            <div class="bg-danger">
              <a href="#"><i class="fa fa-github"></i></a>
            </div>
            <div class="bg-success">
              <a href="#"><i class="fa fa-twitter"></i></a>
            </div>
            <div class="bg-warning">
              <a href="#"><i class="fa fa-skype"></i></a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 center">
          <h1 class=" text-center">Our service</h1>
          <ul>
            <li>Lorem ipsum dolor sit amet consectetur.</li>
            <li>Lorem ipsum dolor sit amet consectetur.</li>
            <li>Lorem ipsum dolor sit amet consectetur.</li>
            <li>Lorem ipsum dolor sit amet consectetur.</li>
          </ul>
        </div>
        <div class="col-lg-4 right">
          <h1 class=" text-center">Contact</h1>
          <div class="row item">
            <div class="col-1 text-success"><i class="fa fa-phone"></i></div>
            <div class="col-10">0363000784</div>
          </div>
          <div class="row item">
            <div class="col-1 text-danger"><i class="fa fa-envelope"></i></div>
            <div class="col-10">lethu210398@gmail.com</div>
          </div>
          <div class="row item">
            <div class="col-1 text-warning"><i class="fa fa-map-marker"></i></div>
            <div class="col-10">97 Man Thien Hiep Phu Dictric 9 HCM City</div>
          </div>
        </div>
      </div>
    
    </div>
  </footer>
@endsection